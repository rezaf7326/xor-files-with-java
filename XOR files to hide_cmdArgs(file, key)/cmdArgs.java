import java.io.*;

class cmdArgs{
	
	public static void main(String args[]){
		
		System.out.println("Your target file is: "+args[0]); // the target file address
		System.out.println("Your key code is: "+args[1]); // is your code for XOR operation, like: 45, 55, 60, 80, etc
		
		byte keyCode = Byte.valueOf(args[1]); // will turn code 40,55,60,etc into byte in hex (: 0x40, 0x55, 0x80, ...)
		
		try (FileInputStream fileInput = new FileInputStream(new File(args[0]))) {
			
			byte [] bufferBytes = new byte[1024*256];
			byte [] updateBuffer = new byte[1024*256];
			
			// now I'm taking a chunk of 1024*256 bytes from the target file
			// (by assigning 1024*256 to bufferBytes-array size, which could obviously be bigger or smaller)
			int bytesRead = fileInput.read(bufferBytes);
			
			for(int i = 0; i < bytesRead; i++) {
				updateBuffer[i] = (byte)(bufferBytes[i] ^ keyCode);
			}
			
			FileOutputStream writeOut = new FileOutputStream(new File(args[0]));
			writeOut.write(updateBuffer);
			
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
}